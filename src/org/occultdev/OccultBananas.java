package org.occultdev;

import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.ScriptCategory;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.event.listeners.RenderListener;
import java.awt.*;


@ScriptMeta(name = "Occult's Bones to Bananas",  desc = "Casts bones to bananas for for 136k/hr profit.", developer = "OccultDev", category = ScriptCategory.MONEY_MAKING, version = 0.2)
public class OccultBananas extends Script implements RenderListener {
    private Player local = null;
    public int moneyMade = 0;

    @Override
    public void onStart() {
        //
    }

    @Override
    public int loop() {
        Player local = Players.getLocal();
        if (!local.isMoving() && !local.isAnimating()) {
            if (!Inventory.contains("Bones")) {
                Bank.open(BankLocation.getNearestWithdrawable());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Bank.depositAllExcept("Nature rune", "Earth rune", "Water staff");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(Bank.withdraw("Bones", 26)) {
                    Bank.close();
                }
            }
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (Magic.cast(Spell.Modern.BONES_TO_BANANAS)) {
                moneyMade+= 271;
            }
        }
        return 600; // The rate of repeat is defined by the returning int, this number represents milliseconds. 1000ms = 1 second.
    }

    @Override
    public void onStop() {
        //When the script is stopped the segment of code in this method will be ran once.
    }

    @Override
    public void notify(RenderEvent renderEvent) {

        Graphics g = renderEvent.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(new Color(0, 0, 0, 128));
        g2.fillRect(20, 240, 209, 95);
        g2.setColor(Color.WHITE);
        g2.drawRect(20, 240, 209, 95);
        int x = 25;
        int y = 255;
        FontMetrics metrics = g2.getFontMetrics();
        String stringMoney = Integer.toString(moneyMade);
        g2.setColor(Color.WHITE);
        g2.drawString("Money made: ", x, y);
        g2.setColor(new Color(238, 133, 74));
        int width = metrics.stringWidth("Money made: ");
        g2.drawString(stringMoney, x + width, y);
        g2.setColor(Color.WHITE);
        if (local != null) {
            Color tanColor = new Color(204, 187, 154);
            g2.setColor(tanColor);
            g2.fillRect(9, 459, 100, 15);
        }
    }
}
